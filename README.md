# Proyecto Java Spring subido a Heroku

## Román García

### Notas

- La parte interesante es la integración de Heruku con Spring, que hace sencillo el deploy, SI EL PROYECTO FUE CREADO CON SPRING-BOOT.

- Lo importante es que el proyecto Java haya sido correctamente creado via Initializr (https://start.spring.io/), porque sino el buildpack de Heroku no va a funcionar correctamente.

- Los proyecto debe ser basado en Maven con las dependencias "Spring Web", "Spring Data JPA", "MySQL Driver" y "PostgreSQL Driver".

- El proyecto usa dos bases de datos:

  - Cuando es local, usa MySQL
  - Cuando está en Heroku usa Postgress (porque no pide tarjeta de crédito).

- Como regla general, cuando menos configuración, mejor. Si espeficamos cosas como los drivers a usar en `application.properties`, el buildpack fallará. El buildpack detecta el driver a partir del la URI de la base de datos.

- Al elegir el plugin de Postgress automáticamente se sobreescriben las configuraciones:

```
spring.datasource.url=jdbc:mysql://localhost:3306/heroku-spring?serverTimezone=GMT-3
spring.datasource.username=admin
spring.datasource.password=admin123
```

- Otro punto interesante fue que es la primera vez que escribo código Java sin usar Eclipse. El proyecto generado por el plugin de Eclipse SpringTools 4 no lo pude integrar con Heroku.

### Guía

La guía que seguí es una mezcla de https://devcenter.heroku.com/articles/deploying-spring-boot-apps-to-heroku y este video https://www.youtube.com/watch?v=KDK5xXPJVIg&list=PL5yPEwpqAbiiblp__eDOuCz7P-ULkpF2J&index=2&t=0s

### Deploy

Si se siguió la guía, es suficiente tipear `git push heroku master` para hacer un nuevo deploy del commit que esté en la rama master.

### Instalación de Maven

Se debería tener Maven instalado. Para ello bajarse el archivo .zip de https://www-eu.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin , descomprimirlo en alguna carpeta, y agregar el subdirectorio `bin` en el PATH.

### Instalación de MySQL Community

- Descargar en instalar `Microsoft Visual C++ Redistributable for Visual Studio 2015, 2017 and 2019` de https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads
- Descargar MySQL Community de https://dev.mysql.com/downloads/windows/installer/8.0.html
- Instalar el Server y el Workbench.

### Extensiones del VSCode utilizadas

- VSCODE sugerirá instalar "Java Extension Pack". Hacerlo. Este pack instalará: "Language Support for Java by Red Hat", "Debugger for Java", "Java Test Runner", "Maven for Java", "Java Dependency Viewer" y "Visual Studio IntelliCode".
- Desde la página podemos elegir utilizar los shortcuts de Eclipse.
- Instalar "Install Spring Boot Extension Pack". Este pack instalará: "Spring Boot Tools", "Cloudfoundry Manifest YML Support","Concourse CI Pipeline Editor", "Spring Initializr Java Support" y "Spring Boot Dashboard"

### Repositorios remotos

- Para ver los repositorios remotos de un proyecto usar `git remote -v`
- Para agregar un repositorio remoto usar `git remote add heroku https://git.heroku.com/heroku-maven-spring.git`
