--------------  Esto es para crear un usuario que no sea root --------------------
-- Se hace una sola vez, al instalar mySQL-- Una vez instalado mySQL, defino un usuario admin con los permisos del root
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin123';

GRANT ALL PRIVILEGES
ON *.*
TO 'admin'@'localhost'
WITH GRANT OPTION;

-- Creo la base de datos para el sistema
CREATE SCHEMA `heroku-spring` DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci;

