package nykros.heroku.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import nykros.heroku.demo.dtos.ItemDTO;
import nykros.heroku.demo.services.ItemService;

@RestController()
@RequestMapping("/api/items")
public class ItemController {
	@Autowired
	private ItemService itemService; // Automagicamente creo el service

	// Respondo al verbo POST creando un item
	@CrossOrigin
	@PostMapping
	public ResponseEntity<ItemDTO> create(@RequestBody ItemDTO inputItemDTO) {
		ItemDTO itemDTO = this.itemService.create(inputItemDTO);

		return new ResponseEntity<ItemDTO>(itemDTO, HttpStatus.CREATED);
	}

	// Respondo al verbo GET obteniendo todos los items
	@CrossOrigin
	@GetMapping
	public List<ItemDTO> getAll() {
		return this.itemService.getAll();
	}

}
