package nykros.heroku.demo.dtos;

import java.time.LocalTime;
import nykros.heroku.demo.models.Item;

public class ItemDTO {
  private Long id;
  private String descripcion;
  private LocalTime hora;

  // Constructor: construyo un DTO desde datos del endpoint
  public ItemDTO(String descripcion, LocalTime hora) {
    super();
    this.descripcion = descripcion;
    this.hora = hora;
  }

  // Contructor: construyo desde de item
  public ItemDTO(Item item) {
    super();
    this.id = item.getId();
    this.descripcion = item.getDescripcion();
    this.hora = item.getHora();
  }

  // Getters
  public Long getId() {
    return id;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public LocalTime getHora() {
    return hora;
  }

  // Otros
  @Override
  public String toString() {
    return "ItemDTO [descripcion=" + descripcion + ", hora=" + hora + ", id=" + id + "]";
  }

}