package nykros.heroku.demo.models;

import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String descripcion;

	@Column(nullable = false)
	private LocalTime hora;

	// Constructores
	public Item() {
		super();
	}

	public Item(String descripcion, LocalTime hora) {
		super();
		this.descripcion = descripcion;
		this.hora = hora;
	}

	// Getters
	public Long getId() {
		return id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public LocalTime getHora() {
		return hora;
	}

	// Setters
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
