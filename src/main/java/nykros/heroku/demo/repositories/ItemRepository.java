package nykros.heroku.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import nykros.heroku.demo.models.Item;

// Voila! Definiendo la interface, el @Autowired dentro del servicio 
// se encarga de generar la implementación de esta interface
@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

}
