package nykros.heroku.demo.services;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import nykros.heroku.demo.dtos.ItemDTO;
import nykros.heroku.demo.models.Item;
import nykros.heroku.demo.repositories.ItemRepository;

@Service
public class ItemService {
  @Autowired
  private ItemRepository itemRepository;

  // Guardo un itemDTO en la base de datos.
  // Devuelvo el DTO con el codigo asignado por la base de datos
  public ItemDTO create(ItemDTO itemDTO) {
    Item item = new Item(itemDTO.getDescripcion(), itemDTO.getHora());
    this.itemRepository.save(item);
    return new ItemDTO(item);
  }

  // Devuelvo todos los items de la base de datos
  public List<ItemDTO> getAll() {
    List<Item> items = (List<Item>) this.itemRepository.findAll();
    return items.stream().map(item -> new ItemDTO(item)).collect(Collectors.toList());
  }
}